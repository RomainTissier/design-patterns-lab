package eu.telecomnancy.ui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import eu.telecomnancy.sensor.AbstractObservableSensor;

public class MainWindow extends JFrame {

	private static final long serialVersionUID = 2292973229197754803L;
	private AbstractObservableSensor sensor;
	private SensorView sensorView;

	public MainWindow(AbstractObservableSensor sensor2) {
		this.sensor = sensor2;
		this.sensorView = new SensorView(this.sensor);

		this.setLayout(new BorderLayout());
		this.add(this.sensorView, BorderLayout.CENTER);

		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.pack();
		this.setVisible(true);
	}

}
