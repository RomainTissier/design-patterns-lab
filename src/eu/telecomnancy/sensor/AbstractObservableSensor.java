package eu.telecomnancy.sensor;

import java.util.Observable;

public abstract class AbstractObservableSensor extends Observable implements ISensor {
	@Override
	public abstract void on();

	@Override
	public abstract void off();

	@Override
	public abstract boolean getStatus();

	@Override
	public abstract void update() throws SensorNotActivatedException;

	@Override
	public abstract double getValue() throws SensorNotActivatedException;

}
