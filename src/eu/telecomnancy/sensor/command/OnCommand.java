package eu.telecomnancy.sensor.command;

import eu.telecomnancy.sensor.AbstractObservableSensor;

public class OnCommand extends AbstractCommand {
	public OnCommand(AbstractObservableSensor sensor) {
		super(sensor);
	}

	@Override
	public void Execute() {
		sensor.on();
	}

}
