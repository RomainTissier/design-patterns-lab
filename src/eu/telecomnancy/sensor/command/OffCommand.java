package eu.telecomnancy.sensor.command;

import eu.telecomnancy.sensor.AbstractObservableSensor;

public class OffCommand extends AbstractCommand {
	public OffCommand(AbstractObservableSensor sensor) {
		super(sensor);
	}

	@Override
	public void Execute() {
		sensor.off();
	}

}
