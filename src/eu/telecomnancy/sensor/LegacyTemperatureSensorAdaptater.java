package eu.telecomnancy.sensor;

public class LegacyTemperatureSensorAdaptater extends AbstractObservableSensor {
	private LegacyTemperatureSensor legacyTemperatureSensor;
	double value = 0;

	public LegacyTemperatureSensorAdaptater() {
		legacyTemperatureSensor = new LegacyTemperatureSensor();
	}

	@Override
	public void on() {
		if (!legacyTemperatureSensor.getStatus())
			legacyTemperatureSensor.onOff();
	}

	@Override
	public void off() {
		if (legacyTemperatureSensor.getStatus())
			legacyTemperatureSensor.onOff();
	}

	@Override
	public boolean getStatus() {
		return legacyTemperatureSensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		if (legacyTemperatureSensor.getStatus()) {
			value = legacyTemperatureSensor.getTemperature();
			this.notifyObservers();
		} else
			throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		if (legacyTemperatureSensor.getStatus())
			return value;
		else
			throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");

	}

}
