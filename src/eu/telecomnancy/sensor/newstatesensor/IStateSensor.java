package eu.telecomnancy.sensor.newstatesensor;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public interface IStateSensor {
	public boolean getStatus();

	public void update() throws SensorNotActivatedException;

	public double getValue() throws SensorNotActivatedException;

}
