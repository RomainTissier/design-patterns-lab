package eu.telecomnancy.sensor.newstatesensor;

import eu.telecomnancy.sensor.AbstractObservableSensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class NewStateSensor extends AbstractObservableSensor {
	IStateSensor stateSensor;

	@Override
	public void on() {
		stateSensor = new OnSensor();
	}

	@Override
	public void off() {
		stateSensor = new OffSensor();
	}

	@Override
	public boolean getStatus() {
		return stateSensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		stateSensor.update();
		this.setChanged();
		this.notifyObservers();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return stateSensor.getValue();
	}

}
