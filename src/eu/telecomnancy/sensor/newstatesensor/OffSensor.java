package eu.telecomnancy.sensor.newstatesensor;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public class OffSensor implements IStateSensor {

	@Override
	public boolean getStatus() {
		return false;
	}

	@Override
	public void update() throws SensorNotActivatedException {
		throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		throw new SensorNotActivatedException("Sensor must be activated to get its value.");
	}

}
