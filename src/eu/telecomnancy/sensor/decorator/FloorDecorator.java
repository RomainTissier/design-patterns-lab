package eu.telecomnancy.sensor.decorator;

import eu.telecomnancy.sensor.AbstractObservableSensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class FloorDecorator extends AbstractValueDecorator {

	public FloorDecorator(AbstractObservableSensor decoratedSensor) {
		super(decoratedSensor);
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return Math.rint(decoratedSensor.getValue());
	}

}
