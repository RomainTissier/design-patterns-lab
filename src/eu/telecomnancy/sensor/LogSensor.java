package eu.telecomnancy.sensor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class LogSensor extends AbstractObservableSensor {
	private DateFormat date;
	private Calendar cal;
	private AbstractObservableSensor sensor;

	public LogSensor(AbstractObservableSensor sensor) {
		this.sensor = sensor;
		date = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		cal = Calendar.getInstance();
	}

	@Override
	public void on() {
		sensor.on();
		System.out.println("[" + date.format(cal.getTime()) + "]" + " Sensor turning on");
	}

	@Override
	public void off() {
		sensor.off();
		System.out.println("[" + date.format(cal.getTime()) + "]" + " Sensor turning off");
	}

	@Override
	public boolean getStatus() {
		boolean status = sensor.getStatus();
		System.out.println("[" + date.format(cal.getTime()) + "]" + " Getting sensor status: " + status);
		return status;
	}

	@Override
	public void update() throws SensorNotActivatedException {
		System.out.println("[" + date.format(cal.getTime()) + "]" + "Try Sensor update");
		sensor.update();
		System.out.println("[" + date.format(cal.getTime()) + "]" + "Sensor updated");
		this.setChanged();
		this.notifyObservers();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		double value = sensor.getValue();
		System.out.println("[" + date.format(cal.getTime()) + "]" + " Getting sensor status: " + value);
		return value;
	}

}
