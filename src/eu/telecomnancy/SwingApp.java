package eu.telecomnancy;

import eu.telecomnancy.sensor.AbstractObservableSensor;
import eu.telecomnancy.ui.MainWindow;

public class SwingApp {

	public static void main(String[] args) {
		AbstractObservableSensor sensor;
		try {
			sensor = SensorFactory.create("/eu/telecomnancy/capteur.properties");
			new MainWindow(sensor);
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		/*
		 * = new TemperatureSensor(); FahrenheitDecorator
		 * fahrenheitdecoratedSensor = new FahrenheitDecorator(sensor);
		 * FloorDecorator floordecoratedSensor = new
		 * FloorDecorator(fahrenheitdecoratedSensor); LogSensor logsensor = new
		 * LogSensor(floordecoratedSensor);
		 */
		// new MainWindow(sensor);
	}

}
